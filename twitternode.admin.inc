<?php

/**
 * @file
 * Administrative UI functions for the twitternode module.
 */

/**
 *
 */
function _twitternode_response_code($code = NULL) {
  $responses = array(
    100 => 'Continue', 101 => 'Switching Protocols',
    200 => 'OK', 201 => 'Created', 202 => 'Accepted', 203 => 'Non-Authoritative Information', 
      204 => 'No Content', 205 => 'Reset Content', 206 => 'Partial Content',
    300 => 'Multiple Choices', 301 => 'Moved Permanently', 302 => 'Found', 303 => 'See Other', 
      304 => 'Not Modified', 305 => 'Use Proxy', 307 => 'Temporary Redirect',
    400 => 'Bad Request', 401 => 'Unauthorized', 402 => 'Payment Required', 403 => 'Forbidden', 
      404 => 'Not Found', 405 => 'Method Not Allowed', 406 => 'Not Acceptable', 
      407 => 'Proxy Authentication Required', 408 => 'Request Time-out', 409 => 'Conflict', 
      410 => 'Gone', 411 => 'Length Required', 412 => 'Precondition Failed', 
      413 => 'Request Entity Too Large', 414 => 'Request-URI Too Large', 
      415 => 'Unsupported Media Type', 416 => 'Requested range not satisfiable', 
      417 => 'Expectation Failed',
    500 => 'Internal Server Error', 501 => 'Not Implemented', 502 => 'Bad Gateway', 
      503 => 'Service Unavailable', 504 => 'Gateway Time-out', 505 => 'HTTP Version not supported',
  );
  return ($code) ? $responses[$code] : FALSE;
}

/**
 *
 */
function twitternode_admin() {
  // build the admin accounts table
  $output = _twitternode_admin_accounts();
  
  // include the settings form on this page
  $settings = variable_get('twitternode_settings', array());
  if (!is_array($settings)) $settings = array();
  $output .= drupal_get_form('twitternode_admin_settingsform', $settings);
  
  return $output;
}

/**
 *
 */
function _twitternode_admin_accounts() {
  $accounts = _twitternode_account_get();
  
  $header = array(
    t('Account'),
    t('Enabled'),
    t('Last Checked'),
    t('Response'),
    t('Operations'),
  );
  
  $rows = array();
  if (is_array($accounts)) {
    foreach ($accounts as $accountid => $accountinfo) {
      $rows[] = array(
        $accountid,
        ($accountinfo['enabled']) ? t('Enabled') : t('Disabled'),
        ($accountinfo['last_checked']) ? format_date($accountinfo['last_checked'], 'medium') : t('Never'),
        ($accountinfo['last_response']) ? $accountinfo['last_response'] . ' ' . _twitternode_response_code($accountinfo['last_response']) : t('None'),
        theme('links', array(
          array('href' => 'admin/settings/twitternode/edit/' . $accountid, 'title' => t('Edit')),
          array('href' => 'admin/settings/twitternode/delete/' . $accountid, 'title' => t('Delete')),
          array('href' => _twitternode_download_url($accountid, $accountinfo), 'title' => t('Next Download')),
        )),
      );
    }
  }
  
  $rows[] = array(
    array(
      'colspan' => count($header),
      'data' => theme('item_list', array(l(t('Add Account'), 'admin/settings/twitternode/add'))),
    ),
  );
  
  $output = theme('table', $header, $rows);
  
  return $output;
}

/**
 *
 */
function _twitternode_admin_get_users() {
  $sql = "SELECT DISTINCT u.uid, u.name FROM {users} u WHERE u.status = 1 ORDER BY u.name ASC";
  $result = db_query($sql);
  $users = array(
    0 => t('None selected'),
  );
  while ($data = db_fetch_object($result)) {
    $users[$data->uid] = $data->name;
  }
  return $users;
}

/**
 *
 */
function twitternode_admin_accountform(&$form_state, $accountid = NULL) {
  
  // load accountinfo for the specified accountid
  if ($accountid) {
    $accountinfo = _twitternode_account_get($accountid);
    
    // if we specify accountid, only allow pre-existing accountid
    if (!$accountinfo['accountid']) {
      drupal_goto('admin/settings/twitternode');
      return FALSE;
    }
  }

  $form['addnew'] = array(
    '#type' => 'value',
    '#value' => !isset($accountid),
  );
  
  if ($accountid) {
    $form['accountid'] = array(
      '#type' => 'value',
      '#value' => $accountid,
    );
  }
  else {
    $form['accountid'] = array(
      '#type' => 'textfield',
      '#title' => t('Twitter username'),
      '#description' => t('The username of the Twitter account to be checked.'),
      '#required' => TRUE,
      '#default_value' => $accountid,
    );
  }
  
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#description' => t('Enabled accounts are included in the Twitternode cron run.'),
    '#default_value' => ($accountid) ? $accountinfo['enabled'] : 0,
  );
  $form['uid'] = array(
    '#type' => 'select',
    '#title' => t('Drupal user'),
    '#description' => t('All posts pulled from this Twitter account will be created by this user.'),
    '#options' => _twitternode_admin_get_users(),
    '#default_value' => ($accountid) ? $accountinfo['uid'] : 0,
  );
  
  $form['defaults'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default publishing options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );
  $form['defaults']['published'] = array(
    '#type' => 'checkbox',
    '#title' => t('Mark new nodes as published'),
    '#default_value' => ($accountid) ? $accountinfo['settings']['defaults']['published'] : 0,
  );
  $form['defaults']['promoted'] = array(
    '#type' => 'checkbox',
    '#title' => t('Promote new nodes to front page'),
    '#default_value' => ($accountid) ? $accountinfo['settings']['defaults']['promoted'] : 0,
  );
  $form['defaults']['sticky'] = array(
    '#type' => 'checkbox',
    '#title' => t('New nodes are sticky at top of lists'),
    '#default_value' => ($accountid) ? $accountinfo['settings']['defaults']['sticky'] : 0,
  );
  
  $form['title'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default node title options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );
  $form['title']['action'] = array(
    '#type' => 'select',
    '#title' => t('Node title action'),
    '#options' => array(
      'trim' => t('Set node title to status update'),
      'date' => t('Set node title to formatted date (default format)'),
      'timestamp' => t('Set node title to timestamp (raw integer value)'),
      'string' => t('Set node title to specified string value'),
    ),
    '#default_value' => ($accountid) ? $accountinfo['settings']['title']['action'] : 'trim',
  );
  $form['title']['trim'] = array(
    '#type' => 'textfield',
    '#title' => t('Character limit of status update for node title'),
    '#description' => t('When using the status update to set the node title, trim the status update to this many characters. Leave blank or zero (0) for maximum allowed by database.'),
    '#size' => 10,
    '#default_value' => ($accountid) ? $accountinfo['settings']['title']['trim'] : 50,
  );
  $form['title']['string'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom string value for node title'),
    '#description' => t('Specify the string value for the node title. Used when node title action is set to string value.'),
    '#default_value' => ($accountid) ? $accountinfo['settings']['title']['string'] : t('Twitter status update'),
  );
  
  $form['replies'] = array(
    '#type' => 'fieldset',
    '#title' => t('"@" replies'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );
  $form['replies']['action'] = array(
    '#type' => 'radios',
    '#title' => t('Default action'),
    '#options' => array(
      'normal' => t('Post node normally'),
      'unpublished' => t('Post node normally, but mark as unpublished'),
      'ignore' => t('Ignore "@" replies: do not import at all'),
    ),
    '#default_value' => ($accountid) ? $accountinfo['settings']['replies']['action'] : 'normal',
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

/**
 *
 */
function twitternode_admin_accountform_validate($form, &$form_state) {
  if (preg_match('/\W/', $form_state['values']['accountid']) == 1) {
    form_set_error('accountid', t('Invalid Twitter username.'));
  }
  else {
    if ($form_state['values']['addnew']) {
      $accountinfo = _twitternode_account_get($form_state['values']['accountid']);
      if ($accountinfo['accountid']) {
        form_set_error('accountid', t('Twitter username already exists in database.'));
      }
    }
    if (!is_numeric($form_state['values']['title']['trim'])) {
      form_set_error('trim', t('Character limit must be a number.'));
    }
  }
}

/**
 *
 */
function twitternode_admin_accountform_submit($form, &$form_state) {
  $accountid = check_plain($form_state['values']['accountid']);
  
  $accountinfo = array();

  $accountinfo['uid'] = $form_state['values']['uid'];
  $accountinfo['enabled'] = $form_state['values']['enabled'];
  
  // force account to be disabled if no uid specified
  $accountinfo['enabled'] = ($accountinfo['uid'] == 0) ? FALSE : $form_state['values']['enabled'];
  
  $accountinfo['settings'] = array(
    'defaults' => $form_state['values']['defaults'],
    'title' => $form_state['values']['title'],
    'replies' => $form_state['values']['replies'],
  );
  
  // check_plain the user-supplied node title string value
  $accountinfo['settings']['title']['string'] = check_plain($accountinfo['settings']['title']['string']);

  // save the new account values to the database
  _twitternode_account_save($accountid, $accountinfo);
  
  drupal_set_message(t('Account saved.'));
  $form_state['redirect'] = 'admin/settings/twitternode';
}

/**
 *
 */
function twitternode_admin_accountdelform(&$form_state, $accountid) {
  return array(
    'accountid' => array(
      '#type' => 'value',
      '#value' => $accountid,
    ),
    'submit' => array(
      '#type' => 'submit',
      '#prefix' => t('Are you sure you want to delete this account?') . '<br />',
      '#value' => t('Delete'),
    ),
  );
}

/**
 *
 */
function twitternode_admin_accountdelform_submit($form, &$form_state) {
  $accountid = $form_state['values']['accountid'];
  if ($accountid) {
    $sql = "DELETE FROM {twitternode_accounts} WHERE accountid = '%s'";
    $result = db_query($sql, $accountid);
    if (result) {
      drupal_set_message(t('Account deleted.'));
    }
    else {
      drupal_set_message(t('An error occurred while attempting to delete account.'), 'warning');
    }
  }
  $form_state['redirect'] = 'admin/settings/twitternode';
}

/**
 *
 */
function twitternode_admin_runonce() {
  if (twitternode_cron(TRUE)) {
    drupal_set_message(t('Twitternode cron process completed.'));
  }
  else {
    drupal_set_message(t('Twitternode cron process returned an error. Please check the logs.'), 'error');
  }
  drupal_goto('admin/settings/twitternode');
}

/**
 *
 */
function twitternode_admin_settingsform(&$form_state, $settings) {
  $form['frequency'] = array(
    '#type' => 'select',
    '#title' => t('Frequency'),
    '#description' => t('How often Twitternode should pull new data from Twitter.'),
    '#options' => array(
      'never' => 'Never (disabled)',
      'manual' => 'Manual imports only',
      'always' => 'Every time cron runs',
    ),
    '#default_value' => (isset($settings['frequency'])) ? $settings['frequency'] : 0,
  );
  $form['manual'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="form-item">',
    '#value' => l('Run next cron run manually', 'admin/settings/twitternode/run-once'),
    '#suffix' => '</div>',
  );
  $form['save_xml'] = array(
    '#type' => 'checkbox',
    '#title' => t('Save XML from each cron run'),
    '#description' => t('If enabled, the unprocessed XML from each cron run is stored in the database. This preserves data integrity and provides a means for further processing. However, it does add considerably more data to the database and may slightly affect cron performance.'),
    '#default_value' => (isset($settings['save_xml'])) ? $settings['save_xml'] : 0,
  );
  
  if (module_exists('taxonomy')) {
    $form['taxonomy'] = array(
      '#type' => 'fieldset',
      '#title' => t('Taxonomy settings'),
      '#description' => t('When enabled, Twitternode can utilize Taxonomy to provide additional metadata for each status update.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    );
    $form['taxonomy']['action'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Create terms for'),
      '#options' => array(
        'account' => t('Twitter account name'),
        'source' => t('Source (e.g. "web", "txt")'),
        'favorited' => t('When favorited (e.g., "favorited")'),
      ),
      '#default_value' => (isset($settings['taxonomy']['action'])) ? $settings['taxonomy']['action'] : array(),
    );
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

/**
 *
 */
function twitternode_admin_settingsform_submit($form, &$form_state) {
  $settings = variable_get('twitternode_settings', array());
  if (!is_array($settings)) $settings = array();
  
  foreach (array('frequency', 'taxonomy', 'save_xml') as $key) {
    $settings[$key] = $form_state['values'][$key];
  }
  
  variable_set('twitternode_settings', $settings);
  drupal_set_message(t('Changes saved.'));
  $form_state['redirect'] = 'admin/settings/twitternode';
}

/**
 *
 */
function twitternode_admin_xml() {
  $settings = variable_get('twitternode_settings', array());
  if ($settings['save_xml']) {
    drupal_set_message(t('Save XML is enabled.'));
  }
  else {
    drupal_set_message(t('Save XML is disabled. Data from cron runs is not being saved.'), 'warning');
  }
  
  $header = array(
    'Timestamp',
    'Operations',
  );
  $rows = array();
  
  $sql = "SELECT tnx.timestamp FROM {twitternode_xml} tnx ORDER BY tnx.timestamp DESC";
  $result = pager_query($sql, 25);
  while ($data = db_fetch_object($result)) {
    $rows[] = array(
      l(format_date($data->timestamp, 'medium'), 'admin/settings/twitternode/xml/view/' . $data->timestamp),
      theme('links', array(
        array('href' => 'admin/settings/twitternode/xml/view/' . $data->timestamp, 'title' => 'View'),
        array('href' => 'admin/settings/twitternode/xml/delete/' . $data->timestamp, 'title' => 'Delete'),
      )),
    );
  }
  if (count($rows) < 1) {
    $rows[] = array(
      array(
        'colspan' => 2,
        'data' => t('No saved XML found in database.'),
      ),
    );
  }
  
  return theme('table', $header, $rows) . theme('pager');
}

/**
 *
 */
function twitternode_admin_xmlview($timestamp) {
  if (!is_numeric($timestamp) || $timestamp <= 0) {
    drupal_goto('admin/settings/twitternode/xml');
    return FALSE;
  }
  
  $sql = "SELECT tnx.uri, tnx.xml FROM {twitternode_xml} tnx WHERE tnx.timestamp = %d";
  $data = db_fetch_object(db_query($sql, $timestamp));
  
  if (drupal_strlen($data->xml) <= 0) {
    drupal_goto('admin/settings/twitternode/xml');
    return FALSE;
  }
  
  $form['timestamp'] = array(
    '#type' => 'item',
    '#title' => t('Timestamp'),
    '#value' => format_date($timestamp, 'medium'),
    '#weight' => 0,
  );
  $form['uri'] = array(
    '#type' => 'item',
    '#title' => t('URI'),
    '#value' => l($data->uri, $data->uri),
    '#weight' => 1,
  );
  $form['xml'] = array(
    '#type' => 'textarea',
    '#title' => t('XML'),
    '#cols' => 60,
    '#rows' => 20,
    '#resizable' => TRUE,
    '#value' => $data->xml,
    '#weight' => 2,
  );
  
  return drupal_render_form('twitternode_admin_xmlview', $form);
}

/**
 *
 */
function twitternode_admin_xmldel(&$form_state, $timestamp) {
  if (!is_numeric($timestamp) || $timestamp <= 0) {
    drupal_goto('admin/settings/twitternode/xml');
    return FALSE;
  }
  
  $form['timestamp'] = array(
    '#type' => 'value',
    '#value' => $timestamp,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#prefix' => t('Do you really want to delete this XML record?') . '<br />',
  );
  
  return $form;
}

/**
 *
 */
function twitternode_admin_xmldel_submit($form, &$form_state) {
  $sql = "DELETE FROM {twitternode_xml} WHERE timestamp = %d";
  $result = db_query($sql, $form_state['values']['timestamp']);
  if ($result) {
    drupal_set_message(t('XML record deleted.'));
  }
  else {
    drupal_set_message(t('Encountered database error when attempting to delete XML record.'), 'error');
  }
  $form_state['redirect'] = 'admin/settings/twitternode/xml';
}
